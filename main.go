package main

import (
	"flag"
	"io/ioutil"
	"fmt"
	"os"
	"encoding/json"
	"math"
	"os/exec"
	"regexp"
)

type Config struct {
	Url   []interface{}
	Tests []Test
}

type Test struct {
	Name   string
	Params map[string]interface{}
}

func main() {
	var configPath string
	flag.StringVar(&configPath, "url", "./bh-config.json", "path to bh config json file")
	flag.Parse()

	config := loadConfig(configPath)

	urls := config.BuildUrls()

	fmt.Println(urls)
	for _, url := range urls {
		var err error
		cmdName := "/Applications/Google Chrome.app/Contents/MacOS/Google Chrome"
		cmdArgs := []string{"--headless", "--disable-gpu", "--screenshot", url}
		if _, err = exec.Command(cmdName, cmdArgs...).Output();
			err != nil {
			fmt.Fprintln(os.Stderr, "Error: ", err)
			os.Exit(1)
		}

		r, _ := regexp.Compile("/+")

		cmdName = "mv"
		cmdArgs = []string{"./screenshot.png", "./screenshots/" + r.ReplaceAllString(url, "-") + ".png"}
		if _, err = exec.Command(cmdName, cmdArgs...).Output(); err != nil {
			fmt.Fprintln(os.Stderr, "Error: ", err)
			os.Exit(1)
		}
	}

	//sha := string(cmdOut)
	//fmt.Println("out: ", sha)
}

func loadConfig(configPath string) Config {
	raw, err := ioutil.ReadFile(configPath)
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
	var config Config
	json.Unmarshal(raw, &config)
	return config
}

func (c *Config) BuildUrls() []string {
	var outUrls []string

	product := 1

	var segLens []int

	for _, url := range c.Url {
		if _, ok := url.(string); !ok {
			product = product * len(url.([]interface{}))
		}
	}

	p := 1
	for _, url := range c.Url {
		if _, ok := url.(string); !ok {
			p = p * len(url.([]interface{}))
			segLens = append(segLens, product/p)
		}
	}

	for i := 0; i < product; i++ {
		out := ""
		j := 0
		for _, url := range c.Url {
			if _, ok := url.(string); ok {
				out = out + url.(string)
			} else {
				index := int(math.Floor(float64(i/segLens[j]))) % len(url.([]interface{}))
				out = out + url.([]interface{})[index].(string)
				j++
			}
		}
		outUrls = append(outUrls, out)
	}

	return outUrls
}
